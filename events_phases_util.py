import os, datetime, re
import pandas as pd; from pandas import Series, DataFrame
import numpy as np
from zipfile import ZipFile

def parse_events_in_counts_zip(counts_zip, print_output=True, output_file=None):
    """
    Parse events embedded in a daily counts zip, such as might be downloaded
    via SiteAPI.get_counts_by_date, and optionally print them to the console
    and/or write them to a file.

    Parameters
    ----------
    counts_zip : string
        Full path to a daily counts zip file
    print_output : boolean (optional, default = True)
        If true, print the events of interest to the console.
    output_file : string (optional, default = None)
        If provided, should be full path to file where events will be written.

    Returns
    -------
    N/A
    """

    events_of_interest = {
        200: 'server on',
        205: 'update started',
        210: 'engine on',
        211: 'engine off',
        216: 'flash',
        220: 'camera online',
        221: 'camera offline',
        230: 'camera loss vis',
        231: 'camera regain vis',
        232: 'cameras rebooted',
        239: 'user auth failed',
        240: 'publish',
        241: 'reboot called'
    }

    event_codes_of_interest = events_of_interest.keys()
    zf = ZipFile(counts_zip)

    events_csv_in_zip = None
    for nn in zf.namelist():
        if nn.startswith('events'):
            events_csv_in_zip = nn
            break

    if events_csv_in_zip is None:
        print 'No events file found in %s' % counts_zip
        return

    evts = pd.read_csv(
            zf.open(events_csv_in_zip, 'r'),
            index_col=False,
            names=['time','code','payload'],
            dtype={'time':np.str,'code': np.int, 'payload': np.str}
        )

    # get just events of interest
    evts = evts[ evts['code'].isin(event_codes_of_interest) ]

    # fill missing values with empty string
    evts = evts.fillna('')

    date_regex = '[0-9]{4}-[0-9]{2}-[0-9]{2}'
    date_str = re.search(date_regex, events_csv_in_zip).group()
    date_str = date_str.replace('-','') # remove dashes
    newline = '\n'
    output = ''

    for row_idx, row in evts.iterrows():
        # timestamp from date, removing partial seconds
        timestamp = date_str + 'T' + row['time'].partition('.')[0]

        # what was the event
        evt_str = events_of_interest[ row['code'] ]

        # what was the payload
        evt_payload = row['payload']

        output = output + timestamp + ', ' + evt_str + ' [' + str(row['code']) + '], ' + evt_payload + newline

    if print_output:
        print '---'
        if len(output) == 0:
            output = 'no noteworthy events for %s' % counts_zip
        print output
        print '---'


    if output_file is not None:
        with open(output_file, 'w') as fout:
            fout.write(output)


def events_dataframe_from_csv(events_csv, keep_only_8_phases = True):
    """
    Create a DataFrame for the daily events in the specified file

    Parameters
    ----------
    events_csv : string
        Full path to a single-day events CSV file whose filename must be of
        form YYYY-MM-DD.csv
    keep_only_8_phases : bool (optional, default = True)
        Keep only the first eight phases of phase events or all of the phases

    Returns
    -------
    df : DataFrame
        pandas DataFrame of all events whose index will be of DatetimeIndex
        type
    """

    date_str = os.path.splitext(os.path.basename(events_csv))[0]

    events_columns, events_types = events_columns_and_types()

    df = pd.read_csv(events_csv, header=None, names=events_columns, dtype=events_types)

    timestamps = pd.to_datetime( date_str + ' ' + df['time_str'])
    timestamps.name = None
    df.index = timestamps
    df.drop('time_str', axis=1, inplace=True)

    if keep_only_8_phases:
        phases = (df['event']==phases_event())
        df.loc[phases,'payload'] = df['payload'][phases].map(lambda s: str(s)[0:8])

    return df


def phase_transitions_from_events_dataframe(events_df, seconds_of_yellow = 4.):
    """
    Get the phases and transition times from the provided events DataFrame

    Parameters
    ----------
    events_df : DataFrame
        Events in form such as returned from `events_dataframe_from_csv`
    seconds_of_yellow : float (optional, default = 4.)
        Assumed seconds of yellow for making G-Y-R from only binary phases data

    Returns
    -------
    phases : dict of {phase:Series}
        Returns a dictionary keyed by phase numbers 1-8 where the values
        are pandas Series objects with Datetime index and values 'R', 'G', or 'Y'
        indicating the phase state that occured at the index time
    """


    # just the phases
    phases_df = events_df[ events_df['event']==phases_event() ]

    phase_transitions = {}
    phase_states = {}
    raw_phase_states = {}

    for p in range(0,8):
        phase = p+1
        phase_transitions[phase] = []
        phase_states[phase] = []
        raw_phase_states[phase] = []

    for timestamp, row in phases_df.iterrows():

        for p in range(0,8):
            phase = p+1
            raw_state = int(row['payload'][p])

            if (len(phase_transitions[phase])==0):

                # initialize if empty, assume 0 means red at init
                phase_transitions[phase].append(timestamp)
                raw_phase_states[phase].append(raw_state)

                if raw_state==0:
                    phase_states[phase].append('R')
                else:
                    phase_states[phase].append('G')

            elif (raw_phase_states[phase][-1] != raw_state):

                # if state changed, append timestamp and raw state
                phase_transitions[phase].append(timestamp)
                raw_phase_states[phase].append(raw_state)

                rcvd_green = (raw_state==1)
                if rcvd_green:
                    phase_states[phase].append('G')
                else:
                    phase_states[phase].append('Y')
                    t_yellow_ends = timestamp + datetime.timedelta(seconds=seconds_of_yellow)
                    phase_transitions[phase].append(t_yellow_ends)
                    phase_states[phase].append('R')

    # put data into a Series for each phase, all in a dictionary
    phases = {}
    for p in range(0,8):
        phase = p+1
        phases[phase] = pd.Series(phase_states[phase], index=phase_transitions[phase])

    return phases


def flow_seconds_for_phase_at_time(phase_transitions, t):
    """
    Compute the number of seconds a phase has been allowing flow for a given time

    Parameters
    ----------
    phase_transitions : Series
        Datetime-indexed pandas Series of 'R', 'G', 'Y' values such as returned
        in the dictionary result of `phase_transitions_from_events_dataframe`
    t : Datetime
        Datetime for which to compute the flow seconds

    Returns
    -------
    flow_seconds : float
        Total seconds of flow, calculated as time since the most recent green
        transition if t is during 'G' or 'Y'. Returns 0 if t is during 'R'
    """

    states_before_t = (phase_transitions.index <= t)
    greens_before_t = ((phase_transitions.index <= t) & (phase_transitions == 'G'))

    if np.count_nonzero(states_before_t)==0 or np.count_nonzero(greens_before_t)==0:
        return -1.0

    state_idx_for_t = max( phase_transitions.index[states_before_t] )
    state_for_t = phase_transitions[state_idx_for_t]

    if state_for_t == 'R':
        return 0.0

    t_green = max( phase_transitions.index[greens_before_t] )
    flow_time = t - t_green

    return flow_time.total_seconds()


def cycle_lengths_for_phases(phases, between_reds=True):
    """
    Compute the number of seconds between greens for a given phase or phases.

    Parameters
    ----------
    phases : dict of {phase:Series}
        Dictionary keyed by phase numbers 1-8 where the values are pandas
        Series objects with Datetime index and values 'R', 'G', or 'Y'
        indicating the phase state that occured at the index time, as would
        be returned by phase_transitions_from_events_dataframe
    between_reds : boolean (default = True)
        If True, measure cycle length as seconds between start of reds, else
        measure as seconds between start of greens.

    Returns
    -------
    cycle_lengths : dict of {phase:Series}
        Dictionary keyed by phase numbers 1-8 where the values are pandas
        Series objects with Datetime index and value of seconds. The index
        corresponds to the start of the phase green and the value in seconds
        corresponds to the time until the next green.
    """

    key = 'G'
    if between_reds:
        key = 'R'
    cycle_lengths = {}
    for (phase,phase_transitions) in phases.iteritems():
        cycle_starts = phase_transitions[(phase_transitions==key)]
        seconds_between_starts = (cycle_starts.index[1:]-cycle_starts.index[:-1]).total_seconds()

        cycle_lengths[phase] = pd.Series(seconds_between_starts, index=cycle_starts.index[:-1])

    return cycle_lengths


def green_lengths_for_phases(phases):
    """
    Compute the number of seconds between greens for a given phase or phases.

    Parameters
    ----------
    phases : dict of {phase:Series}
        Dictionary keyed by phase numbers 1-8 where the values are pandas
        Series objects with Datetime index and values 'R', 'G', or 'Y'
        indicating the phase state that occured at the index time, as would
        be returned by phase_transitions_from_events_dataframe

    Returns
    -------
    green_lengths : dict of {phase:Series}
        Dictionary keyed by phase numbers 1-8 where the values are pandas
        Series objects with Datetime index and value of seconds. The index
        corresponds to the start of the phase green and the value in seconds
        corresponds to the time until the next green.
    """

    green_lengths = {}
    for (phase,phase_transitions) in phases.iteritems():

        # where greens for this phase start
        green_starts_ind = (phase_transitions=='G')

        # if last entry of day is green start, don't use it
        # b/c we don't have it's end on next day
        green_starts_ind[-1] = False

        green_start_times = []
        green_length_seconds = []

        for ii in len(green_starts_ind):
            if not green_starts_ind[ii]:
                continue

            seconds_of_green = (phase_transitions.index[ii+1] - phase_transitions.index[ii]).total_seconds()
            green_start_times.append(phase_transitions.index[ii])
            green_length_seconds.append(seconds_of_green)

        green_lengths[phase] = pd.Series(green_length_seconds, index=green_start_times)

    return green_lengths



def flow_seconds_per_cycle_for_phase(phase_transitions):
    """
    Compute the number of seconds of flow (green time plus yellow time) for each
    green occurence of the phase

    Parameters
    ----------
    phase_transitions : Series
        Datetime-indexed pandas Series of 'R', 'G', 'Y' values such as returned
        in the dictionary result of `phase_transitions_from_events_dataframe`

    Returns
    -------
    flow_seconds : Series
        Total seconds of flow, calculated as time between green start and
        yellow end, indexed by datetime that the green started.
    """

    # remove edge cases
    pc = phase_transitions.copy()
    if pc[0] == 'G':
        pc = pc[1:]
    if pc[-1] == 'Y':
        pc = pc[:-2]
    elif pc[-1] == 'G':
        pc = pc[:-1]

    g_start_t = []
    g_flow_sec = []
    g_active = False
    for tt in range(0,len(pc)):

        if pc[tt]=='G':
            g_active = True
            g_start_t.append(pc.index[tt])

        elif pc[tt]=='R' and g_active:
            g_active = False
            g_flow_sec.append( (pc.index[tt] - g_start_t[-1]).total_seconds() )

    return pd.Series(g_flow_sec, index=g_start_t)


def phases_event():
    return 215


def events_columns_and_types():
    event_columns = ['time_str', 'event', 'payload']
    event_types = {'time_str':np.str, 'event':np.int, 'payload':np.str}
    return event_columns, event_types
