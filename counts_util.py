import os, shutil, datetime
from zipfile import ZipFile
import numpy as np
import pandas as pd; from pandas import Series, DataFrame
from gtipy.general_util import *
from gtipy.events_phases_util import *
from primary_phase_diviner import primary_phase_for_zone


def make_daily_count_per_line_csvs(site_path, zones, site_timezone, root_output_path=None, scratch_root = '_scratch', do_cleanup = True):
    """
    Parse a directory of daily site data zip files, such as would be created by
    api_util.get_site_data, and create a single CSV for each day where each
    line of that CSV represents a single count event with columns:
        timestamp
        approach
        turn
        length_ft
        speed_mph
        phase
        light
        seconds_of_light_state
        seconds_since_green
        recent_free_flow_speed_mph
        calibration_free_flow_speed_mph
        include_in_approach_data
        zone_id


    Parameters
    ----------
    site_path : string (name must be a site ID)
        Path to site directory, named by site ID, wherein are contained daily data zip files
        inside a 'zips' subdirectory as is created by site_api.get_counts_by_date default
        behavior, e.g., ./<site_id>/zips
    zones : dict
        Dictionary of zone info such as would be returned by site_api.get_vehicle_zones
    site_timezone : pytz.timezone
        Timezone of the site, e.g., pytz.timezone('US/Eastern')
    root_output_path : string (default = site_path)
        Path to <root> output directory where daily count CSV files will be stored as
        <root>/<site_id>/dailys/YYYY-MM-DD.csv and event CSV files will be stored as
        <root>/<site_id>/events/YYYY-MM-DD.csv
    scratch_root : string (default = './_scratch')
        Path to a scratch directory where temporary files will be placed
    do_cleanup : boolean (default = True)
        Clean up files in the scratch path

    Returns
    -------
    event_count : Int
        Total number of count events parsed.
    """

    event_count = 0
    site_path = os.path.abspath(site_path)
    site_id = os.path.split(site_path)[1]
    
    zips_path = os.path.join(site_path, 'zips')
    site_day_zips = regex_matching_files_in_dir(zips_path, regex_pattern = regex_pattern_for_date(), match_ext = '.zip')

    count_folder_zip_pattern = regex_pattern_for_mac_address() + '/' + regex_pattern_for_date() + '.zip'
    events_file_zip_pattern = 'events/' + regex_pattern_for_date() + '.csv'

    if root_output_path is None:
        root_output_path = os.path.abspath(os.curdir)
    create_dir(root_output_path)

    site_output_path = os.path.join( os.path.join(root_output_path), site_id )
    dailys_output_path = os.path.join( site_output_path, 'dailys' )
    create_dir(dailys_output_path)
    # print 'creating ', dailys_output_path
    events_output_path = os.path.join( site_output_path, 'events' )
    create_dir(events_output_path)
    # print 'creating ', events_output_path


    create_dir(scratch_root)
    scratch_path = os.path.join( os.path.abspath(scratch_root), site_id )
    create_dir(scratch_path)

    for site_day_zip in site_day_zips:

        # get the dates for input as well as previous and next days to reduce file open/close calls
        in_day_str = os.path.splitext( os.path.split(site_day_zip)[1] )[0]

        in_day = date_from_date_string(in_day_str)
        in_day_output_csv = os.path.join(dailys_output_path, in_day_str + '.csv')

        prev_day = in_day - datetime.timedelta(1)
        prev_day_str = '%04d-%02d-%02d' % (prev_day.year, prev_day.month, prev_day.day)
        prev_day_output_csv = os.path.join(dailys_output_path, prev_day_str + '.csv')

        next_day = in_day + datetime.timedelta(1)
        next_day_str = '%04d-%02d-%02d' % (next_day.year, next_day.month, next_day.day)
        next_day_output_csv = os.path.join(dailys_output_path, next_day_str + '.csv')

        # these will be opened lazily below
        in_day_fout = None
        prev_day_fout = None
        next_day_fout = None

        try:
            site_day_zf = ZipFile(site_day_zip, 'r')
        except Exception, e:
            msg = '%s,%s' % (site_id, in_day_str)
            print 'bad zip %s' % (msg)
            os.remove(site_day_zip)
            with open('bad_zips.csv', 'a') as f:
                f.write(msg + '\n')
            continue

        # we need the events parsed first (if they exist, we'll use them)
        phases = None
        for (name, info) in zip(site_day_zf.namelist(), site_day_zf.infolist()):

            # only parse this file if it matches the events file pattern
            me = re.search(events_file_zip_pattern, name)
            if me is None:
                continue

            # extract the events CSV at <scratch>/site_id/events/yyyy-mm-dd.csv
            events_csv = site_day_zf.extract(info, site_output_path)
            events_df = events_dataframe_from_csv(events_csv)
            phases = phase_transitions_from_events_dataframe(events_df)


        for (name, info) in zip(site_day_zf.namelist(), site_day_zf.infolist()):

            # only parse this file if it matches the count zip pattern or the events CSV pattern
            mc = re.search(count_folder_zip_pattern, name)
            if mc is None:
                continue

            # extract the daily count zip to <scratch>/site_id/ (set above now in events parsing)
            site_day_counts_zip = site_day_zf.extract(info, scratch_path)

            # extract the zone CSVs to <scratch>/site_id/MAC/yyyy-mm-dd/*.csv
            mac_path = os.path.split(site_day_counts_zip)[0]
            mac_day_path_for_csvs = os.path.join( mac_path, in_day_str )
            site_day_counts_zf = ZipFile(site_day_counts_zip, 'r')
            site_day_counts_zf.extractall(mac_day_path_for_csvs)

            # get the list of zone CSVs
            zone_csvs = regex_matching_files_in_dir(mac_day_path_for_csvs,
                                                    regex_pattern=regex_pattern_for_site_or_zone_id(),
                                                    match_ext = '.csv')

            for zone_csv in zone_csvs:
                zone_id = os.path.splitext(os.path.basename(zone_csv))[0]

                # make sure we have zone info
                have_zone = zones.keys().count(zone_id)>0
                if not have_zone:
                    print 'cannot find zone, skipping data, ' + site_id + ',' + zone_id + ',' + in_day_str
                    continue

                with open(zone_csv, 'r') as f:
                    lines = f.readlines()

                    for line in lines:
                        zone = zones[zone_id]
                        (out_line, out_day) = parse_count(line, in_day_str, zone, site_timezone, phases)

                        # on to next line if parsing failed
                        if out_line is None or out_day is None:
                            continue

                        # set the output file based on the output day
                        fout = None

                        if out_day == in_day:
                            if in_day_fout is None:
                                in_day_fout = open_daily_csv(in_day_output_csv)

                            fout = in_day_fout

                        elif out_day == prev_day:

                            if prev_day_fout is None:
                                prev_day_fout = open_daily_csv(prev_day_output_csv)

                            fout = prev_day_fout

                        elif out_day == next_day:
                            if next_day_fout is None:
                                next_day_fout = open_daily_csv(next_day_output_csv)

                            fout = next_day_fout


                        if fout is None:
                            print 'out_date_fail,%s,%s,%s: %s,%s' % (site_id[:8], zone_id[:8], line.strip(), date_str, date_string_from_date(out_day))
                            continue

                        # record the event
                        fout.write(out_line + '\n')
                        event_count = event_count + 1


            # cleanup (everything but camera MAC folder in scratch)
            if do_cleanup:
                os.remove(site_day_counts_zip)
                shutil.rmtree(mac_day_path_for_csvs)


        # close any open files
        if in_day_fout is not None:
            in_day_fout.close()
        if next_day_fout is not None:
            next_day_fout.close()
        if prev_day_fout is not None:
            prev_day_fout.close()

    return event_count


def open_daily_csv(day_output_csv):
    """
    Opens the site-day CSV file and writes the header if needed
    timestamp,approach,turn,length_ft,speed,phase,light,zone_id
    """


    if os.path.exists(day_output_csv) and not os.path.isfile(day_output_csv):
        raise Exception('open_init_daily_csv: %s exists but is not writable file' % (day_output_csv))

    needs_header = False
    if not os.path.exists(day_output_csv):
        needs_header = True

    f = open(day_output_csv, 'a')

    if needs_header:
        header = (
            'timestamp' + ',' +
            'approach' + ',' +
            'turn' + ',' +
            'length_ft' + ',' +
            'speed_mph' + ',' +
            'phase' + ',' +
            'light' + ',' +
            'seconds_of_light_state' + ',' +
            'seconds_since_green' + ',' +
            'recent_free_flow_speed_mph' + ',' +
            'calibration_free_flow_speed_mph' + ',' +
            'include_in_approach_data' + ',' +
            'zone_id'
            )
        f.write(header + '\n')

    return f


def parse_count(line, date_str, zone, site_timezone, phases = None, strip_nulls = True):
    """
    Parse a single line from a raw count file and return a CSV line that
    contains info about the count and the zone for which it was recorded.

    Parameters
    ----------
    line : string
        Single line from raw count file
    date_str : string
        The day the count file was saved in form YYYY-MM-DD; needed
        because the v7 count files use simplified timestamps w/o dates
    zone : dict
        Dictionary of the zone for which the count was recorded, can
        be acquired from gspy.api.site_api.get_vehicle_zones
    site_timezone : pytz.timezone
        Timezone of the site; needed because we have to adjust v4
        timestamps for UTC.
    phases : dict of {phase:Series} (optional, default = None)
        Dictionary keyed by phase numbers 1-8 where the values
        are pandas Series objects with Datetime index and values 'R', 'G', or 'Y'
        indicating the phase state that occured at the index time, such as
        returned by gspy.data.events_phases_util.phase_transitions_from_events_dataframe
    strip_nulls : boolean (default = True)
        Strip '\x00' from line before parsing; can sometimes be written
        during unexpected Engine exits/shutdowns.

    Returns
    -------
    out_line : string
        Line for output file as:
            timestamp,approach,turn,length_ft,speed,phase,light,zone_id
    out_date : datetime.date
        Date for output count, could be different from input date_str
        after application of UTC offset for v4 counts
    """

    try:
        # strip whitespace
        line = line.strip()

        if strip_nulls:
            line = line.strip('\x00')

        version = float(line.split(',')[0])

        if version == 4.0:
            return parse_v4_count(line, date_str, zone, site_timezone)

        elif version == 7.0:
            return parse_v7_count(line, date_str, zone)

        elif version == 8.0:
            return parse_v8_count(line, date_str, zone)

    except Exception, e:
        print 'parse_count failed %s %s' % (date_str, line.strip())

    return None, None


def parse_v4_count(line, date_str, zone, site_timezone):
    """
    Handles v4 counts for parse_counts. See parse_counts documentation
    for further info.
    """

    try:
        # strip any spaces or newlines
        vals = line.strip().split(',')
        if len(vals) != 16:
            raise Exception()

        date_time_str = vals[2]
        approach_str = zone['ApproachType'][0].encode('ascii')
        turn_str = vals[7]
        length_str = vals[5]
        speed_mph_str = vals[6]
        phase = primary_phase_for_zone(zone)
        light_str = vals[12]
        seconds_of_light_state = str(-1) # doesn't exist in v4
        seconds_since_green = str(-1) # doesn't exist in v4
        recent_free_flow_speed_mph = str(-1) # doesn't exist in v4
        calibration_free_flow_speed_mph = str(-1) # doesn't exist in v4
        include_in_approach_data = str(0)
        if zone['IncludeInData'] is True:
            include_in_approach_data = str(1)
        zone_id = dashify_site_or_zone_id(zone['Id'].encode('ascii'))

        # adjust for UTC time
        utc_timestamp = datetime_from_iso_string(date_time_str)
        utc_offset_seconds = site_timezone.localize(utc_timestamp).utcoffset().total_seconds()
        local_timestamp = utc_timestamp + datetime.timedelta(0, utc_offset_seconds)

        timestamp_str = iso_string_from_datetime(local_timestamp)

        out_line = (
            timestamp_str + ',' +
            approach_str + ','  +
            turn_str + ','  +
            length_str + ',' +
            speed_mph_str + ',' +
            str(phase) + ',' +
            light_str + ',' +
            seconds_of_light_state + ',' +
            seconds_since_green + ',' + 
            recent_free_flow_speed_mph + ',' +
            calibration_free_flow_speed_mph + ',' +
            include_in_approach_data + ',' +
            zone_id
            )

        # v4 files use UTC time, so out_date may be different than the file date
        out_date = local_timestamp.date()

        return out_line, out_date

    except Exception, e:
        print 'parse_v4_count failed on: %s' % (line.strip())
        return None, None


def parse_v7_count(line, date_str, zone):
    """
    Handles v7 counts for parse_counts. See parse_counts documentation
    for further info.
    """

    try:
        # strip any spaces or newlines
        vals = line.strip().split(',')
        if len(vals) != 11:
            raise Exception()


        time_str = vals[2]
        approach_str = zone['ApproachType'][0].encode('ascii')
        turn_str = vals[4]
        length_str = vals[5]
        speed_mph_str = vals[6]
        phase = primary_phase_for_zone(zone)
        light_str = vals[7]
        seconds_of_light_state = str(-1) # doesn't exist in v7
        seconds_since_green = str(-1) # doesn't exist in v7
        recent_free_flow_speed_mph = str(-1) # doesn't exist in v7
        calibration_free_flow_speed_mph = str(-1) # doesn't exist in v7
        include_in_approach_data = str(0)
        if zone['IncludeInData'] is True:
            include_in_approach_data = str(1)
        zone_id = dashify_site_or_zone_id(zone['Id'].encode('ascii'))

        timestamp_str = date_str.replace('-','') + 'T' + time_str

        out_line = (
            timestamp_str + ',' +
            approach_str + ','  +
            turn_str + ','  +
            length_str + ',' +
            speed_mph_str + ',' +
            str(phase) + ',' +
            light_str + ',' +
            seconds_of_light_state + ',' +
            seconds_since_green + ',' + 
            recent_free_flow_speed_mph + ',' +
            calibration_free_flow_speed_mph + ',' +
            include_in_approach_data + ',' +
            zone_id
            )

        # v7 files use local time, so out_date is always same
        out_date = date_from_date_string(date_str)

        return out_line, out_date

    except Exception, e:
        print 'parse_v7_count failed on: %s' % (line.strip())
        return None, None


def parse_v8_count(line, date_str, zone):
    """
    Handles v8 counts for parse_counts. See parse_counts documentation
    for further info.
    """

    try:
        # strip any spaces or newlines
        vals = line.strip().split(',')
        if len(vals) != 14:
            raise Exception()

        time_str = vals[2]
        approach_str = zone['ApproachType'][0].encode('ascii')
        turn_str = vals[4]
        length_str = vals[5]
        speed_mph_str = vals[6]
        phase = primary_phase_for_zone(zone)
        light_str = vals[7]
        seconds_of_light_state = vals[10]
        seconds_since_green = vals[11]
        recent_free_flow_speed_mph = vals[12]
        calibration_free_flow_speed_mph = vals[13]
        include_in_approach_data = str(0)
        if zone['IncludeInData'] is True:
            include_in_approach_data = str(1)
        zone_id = dashify_site_or_zone_id(zone['Id'].encode('ascii'))

        timestamp_str = date_str.replace('-','') + 'T' + time_str

        out_line = (
            timestamp_str + ',' +
            approach_str + ','  +
            turn_str + ','  +
            length_str + ',' +
            speed_mph_str + ',' +
            str(phase) + ',' +
            light_str + ',' +
            seconds_of_light_state + ',' +
            seconds_since_green + ',' + 
            recent_free_flow_speed_mph + ',' +
            calibration_free_flow_speed_mph + ',' +
            include_in_approach_data + ',' +
            zone_id
            )

        # v7 files use local time, so out_date is always same
        out_date = date_from_date_string(date_str)

        return out_line, out_date

    except Exception, e:
        print 'parse_v8_count failed on: %s' % (line.strip())
        return None, None


def columns_for_daily_and_hourly_csvs():
    # note that week is Monday = 0, Sunday = 6 from datetime.date.weekday()
    return ['timestamp', 'day_of_week', 'total', 'nb_left_car', 'nb_left_trk', 'nb_thru_car', 'nb_thru_trk', 'nb_right_car', 'nb_right_trk', 'nb_uturn_car', 'nb_uturn_trk', 'nb_perm_left', 'eb_left_car', 'eb_left_trk', 'eb_thru_car', 'eb_thru_trk', 'eb_right_car', 'eb_right_trk', 'eb_uturn_car', 'eb_uturn_trk', 'eb_perm_left', 'sb_left_car', 'sb_left_trk', 'sb_thru_car', 'sb_thru_trk', 'sb_right_car', 'sb_right_trk', 'sb_uturn_car', 'sb_uturn_trk', 'sb_perm_left', 'wb_left_car', 'wb_left_trk', 'wb_thru_car', 'wb_thru_trk', 'wb_right_car', 'wb_right_trk', 'wb_uturn_car', 'wb_uturn_trk', 'wb_perm_left', 'holiday', 'special_event']


def column_dicts_for_daily_and_hourly_csvs():
    approach_dict = {'nb_':'N', 'eb_':'E', 'sb_':'S', 'wb_':'W'}
    turn_dict = {'left_':'L', 'thru_':'S', 'right_':'R', 'uturn_':'U'}
    return approach_dict, turn_dict


def length_to_split_car_trk():
    return 49. # see http://docs.trb.org/prp/13-2658.pdf


def daily_counts_dataframe_from_csv(daily_csv):
    """
    Get a DataFrame representation of all the counts in the provided file

    Parameters
    ----------
    daily_csv : str
        Full path to a daily CSV file such as that created by
        `make_daily_count_per_line_csvs`

    Returns
    -------
    df : DataFrame
        DataFrame of the count data in the provided CSV file where timestamp
        column is a datetime (rather than raw string)
    """
    df = pd.read_csv(daily_csv)
    df['timestamp'] = pd.to_datetime(df['timestamp'])
    return df


def make_by_day_and_hour_csvs_for_site(site_id, input_path, output_path = '.', log_file = None):
    """
    Parse the daily count-per-line CSVs generated by `make_daily_count_per_line_csvs` and create
    two output CSVs, one where each line represents an hour of data and another where each line
    represents one day of data.

    Parameters
    ----------
    site_id : string
        The site unique ID
    input_path : string
        Path to directory containing the daily CSVs for the site, such as `./<site_id>/dailys`
        if defaults used for `make_daily_count_per_line_csvs`
    output_path : string (default = current working directory)
        Where to write the two output CSV files, `<site_id>_by_hour.csv` and
        `<site_id>_by_day.csv`
    log_file : string (defaults = None)
        Path to optional output log file for analysis as needed.

    Returns
    -------
    (all_days_df, all_hours_df) : (DataFrame, DataFrame) tuple
        The Dataframes of the by-day and by-hour data representing what was written to the
        two output files. The 40 columns of the output files are as defined in
        `columns_for_daily_and_hourly_csvs`. Note that `car` implies anything less than or
        equal to 49 feet in length and `trk` (truck) is anything greater than 49 feet
        in length.
    """

    # get list of count-per-line CSVs in the path
    count_per_line_csvs = regex_matching_files_in_dir(
        query_dir = input_path,
        regex_pattern = regex_pattern_for_date(),
        match_ext = 'csv')

    if len(count_per_line_csvs)==0:
        print 'ERROR no count per line CSVs found in %s' % (input_path)
        return None

    # helpers for populating data
    (approach_dict, turn_dict) = column_dicts_for_daily_and_hourly_csvs()
    car_trk_split = length_to_split_car_trk()

    # hours and days
    counted_days = sorted( map(lambda s: os.path.splitext( os.path.split(s)[1] )[0], count_per_line_csvs) )
    first_date = date_from_date_string(counted_days[0])
    last_date = date_from_date_string(counted_days[-1])

    all_hours = []
    all_days = []

    the_date = first_date
    while the_date <= last_date:
        the_date_str = date_string_from_date(the_date)
        all_days.append(the_date_str.replace('-',''))

        for hh in range(0,24):
            hhmmss_str = '%02d0000' % (hh)
            timestamp = the_date_str.replace('-','') + 'T' + hhmmss_str
            all_hours.append(timestamp)

        the_date = the_date + datetime.timedelta(1)

    # return all_days, all_hours

    # initialize the daily and hourly dataframes
    all_days_df = pd.DataFrame(columns = columns_for_daily_and_hourly_csvs())
    all_days_df['timestamp'] = all_days
    all_days_df['day_of_week'] = map(lambda ds: date_from_date_string(ds).weekday(), all_days_df['timestamp'])
    all_days_df.fillna(0, inplace = True)

    all_hrs_df = pd.DataFrame(columns = columns_for_daily_and_hourly_csvs())
    all_hrs_df['timestamp'] = all_hours
    all_hrs_df['day_of_week'] = map(lambda ds: date_from_date_string(ds).weekday(), all_hrs_df['timestamp'])
    all_hrs_df.fillna(0, inplace = True)

    # return all_days_df, all_hrs_df

    for cpl in count_per_line_csvs:

        # get the YYYY-MM-DD part of filename, which is the date of interest, strip dashes
        day_str = os.path.splitext( os.path.split(cpl)[1] )[0]
        day_timestamp = day_str.replace('-','')

        msg = '%s..., %s' % (site_id[:8], day_str)
        # print msg
        if log_file is not None:
            dts = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            with open(log_file, 'a') as f:
                f.write(dts + ': ' + msg + '\n')

        # boolean indicator of the row for this day
        day_row = (all_days_df['timestamp']==day_timestamp)

        # read in this days counts
        day_counts = pd.read_csv(cpl, error_bad_lines = False, warn_bad_lines = True)

        # generate hour from the ISO timestamp string
        day_counts['hour'] = day_counts['timestamp'].apply(lambda s: datetime_from_iso_string(s).hour)

        for hh in range(0,24):
            hhmmss = '%02d0000' % (hh)
            hr_timestamp = day_timestamp + 'T' + hhmmss

            # get the counts for this hour
            hr_counts = day_counts[ day_counts['hour'] == hh]
            tot_hr = hr_counts.shape[0]

            # boolean indicator of the row for this hour
            hr_row = (all_hrs_df['timestamp']==hr_timestamp)

            all_hrs_df.loc[hr_row, 'total'] = tot_hr
            all_days_df.loc[day_row, 'total'] = all_days_df.loc[day_row, 'total'] + tot_hr

            # N, E, S, W
            for (approach_key,approach_val) in approach_dict.iteritems():
                hr_approach = hr_counts[hr_counts['approach']==approach_val]

                # L, S, R, U
                for (turn_key,turn_val) in turn_dict.iteritems():

                    hr_approach_turn = hr_approach[hr_approach['turn']==turn_val]
                    tot_hr_approach_turn = hr_approach_turn.shape[0]

                    hr_approach_turn_car = hr_approach_turn[hr_approach_turn['length_ft']<=car_trk_split]
                    cars_hr_approach_turn = hr_approach_turn_car.shape[0]
                    trks_hr_approach_turn = tot_hr_approach_turn - cars_hr_approach_turn

                    car_column = approach_key + turn_key + 'car'
                    trk_column = approach_key + turn_key + 'trk'

                    all_hrs_df.loc[hr_row, car_column] = cars_hr_approach_turn
                    all_days_df.loc[day_row, car_column] = all_days_df.loc[day_row, car_column] + cars_hr_approach_turn

                    all_hrs_df.loc[hr_row, trk_column] = trks_hr_approach_turn
                    all_days_df.loc[day_row, trk_column] = all_days_df.loc[day_row, trk_column] + trks_hr_approach_turn

                    # permitted lefts
                    if turn_val == 'L':
                        hr_approach_perm_left = hr_approach_turn[hr_approach_turn['light']=='PG']
                        tot_hr_approach_perm_left = hr_approach_perm_left.shape[0]

                        perm_left_column = approach_key + 'perm_left'
                        all_hrs_df.loc[hr_row, perm_left_column] = tot_hr_approach_perm_left
                        all_days_df.loc[day_row, perm_left_column] = all_days_df.loc[day_row, perm_left_column] + tot_hr_approach_perm_left


    # do the output
    out_path = os.path.abspath(output_path)
    create_dir(out_path)

    days_out_name = site_id + '_by_day.csv'
    days_out_path = os.path.join(out_path, days_out_name)

    hrs_out_name = site_id + '_by_hour.csv'
    hrs_out_path = os.path.join(out_path, hrs_out_name)

    all_days_df.to_csv(days_out_path, index = False)
    all_hrs_df.to_csv(hrs_out_path, index = False)

    msg = '%d counts, %d days, %s...' % (all_days_df['total'].sum(), all_days_df.shape[0], site_id[:8])
    # print msg
    if log_file is not None:
        dts = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        with open(log_file, 'a') as f:
            f.write(dts + ': ' + msg + '\n')

    return all_days_df, all_hrs_df


def count_version_for_count_line(line):
    """
    Determine the count line version from the given count line

    Parameters
    ----------
    line : str
        Single line from an Engine-recorded count file 

    Returns
    -------
    version : float
        Count line version if one of the valid values (4, 5, 7, 8), else throws
    """

    version = float(line.strip().split(',')[0])

    if version == 4.0 or version == 5.0 or version == 7.0 or version == 8.0:
        return version
    else:
        raise Exception('found invalid version %f from %s' % (version,line))


def count_version_for_count_file(zone_counts_file):
    """
    Determine the count line version for the given count file,
    assuming the entire file comprises lines of same count version

    Parameters
    ----------
    zone_counts_file : str
        Full path to the file

    Returns
    -------
    version : float
        Count line version if one of the valid values (4, 5, 7, 8), else throws
    """
    with open(zone_counts_file) as count_file:
        line = count_file.readline()
        return count_version_for_count_line(line)


def count_headers_for_count_line(line):
    """
    Return list of headers for the given count line.
    See CountFileSpecification.md in Specs folder of GRIDSMART/documentation repo

    Parameters
    ----------
    line : str
        Single line from an Engine-recorded count file 

    Returns
    -------
    headers : list of str
        Headers (keys) for CSV data in the count line.
    """
    return count_column_names_for_count_version(count_version_for_count_line(line))


def count_column_names_for_count_file(zone_counts_file):
    """
    Determine the count line headers for the given count file,
    assuming the entire file comprises lines of same count version

    Parameters
    ----------
    zone_counts_file : str
        Full path to the file

    Returns
    -------
    headers : list of str
        Headers (keys) for CSV data in the count line.
    """
    return count_column_names_for_count_version(count_version_for_count_file(zone_counts_file))


def count_column_names_for_count_version(version):
    """
    Return list of headers for the given version of a count line.
    See `counts.md` in `doc` folder.

    Parameters
    ----------
    version : float
        Count line version

    Returns
    -------
    headers : list of str
        Headers (keys) for CSV data in the count line if count line is valid
        version (4, 7, 8), else throws
    """
    if version == 4.0: # v4.0 through 5.4
        return ['count_version','site_revision','utc_datetime_str','vehicle_name','vehicle_type','length_ft','speed_mph','turn','allowable_turns','seconds_in_zone','seconds_since_last_exit','vehicles_remaining_in_zone','light_state','seconds_since_green','frame','day_or_night']

    elif version == 5.0: # Engine R&D version (as of System v6.4)
        return ['count_version', 'site_revision', 'local_time_str', 'utc_offset_minutes', 'turn', 'length_ft', 'speed_mph', 'light_state', 'seconds_in_zone', 'vehicles_remaining_in_zone',  'seconds_of_light_state', 'seconds_since_green','zone_recent_freeflow_speed_mph','zone_calibration_freeflow_speed_mph',
            'seconds_last_green', 'frame', 'vehicle_name']

    elif version == 7.0: # version 5.4 through 6.3
        return ['count_version','site_revision','local_time_str','utc_offset_minutes','turn','length_ft','speed_mph','light_state','seconds_in_zone','vehicles_remaining_in_zone', 'confidence']

    elif version == 8.0: # version 6.4 through current
        return ['count_version','site_revision','local_time_str','utc_offset_minutes','turn','length_ft','speed_mph','light_state','seconds_in_zone','vehicles_remaining_in_zone', 'seconds_of_light_state','seconds_since_green','zone_recent_freeflow_speed_mph','zone_calibration_freeflow_speed_mph']

    else:
        raise Exception("cannot handle version %f" % (version))


def count_column_dtypes_for_count_version(version):
    """
    Return dict of name, dtype pairs corresponding to columns of a count line.
    See `counts.md` in `doc` folder.

    Parameters
    ----------
    version : float
        Count line version

    Returns
    -------
    dtypes : dict of str:dtype
        Dictionary of column name, dtype pairs
    """
    if version == 4.0: # System v4.0 through v5.4
        return {'count_version':np.int, 'site_revision':np.int, 'utc_datetime_str':np.str, 'vehicle_name':np.str, 'vehicle_type':np.str,'length_ft':np.int,'speed_mph':np.int,'turn':np.str, 'allowable_turns':np.str, 'seconds_in_zone':np.float, 'seconds_since_last_exit':np.float, 'vehicles_remaining_in_zone':np.int, 'light_state':np.str, 'seconds_since_green':np.float, 'frame':np.int, 'day_or_night':np.str}

    elif version == 5.0: # Engine R&D version (as of System v6.4)
        return {'count_version':np.int, 'site_revision':np.int, 'local_time_str':np.str, 'utc_offset_minutes':np.float, 'turn':np.str, 'length_ft':np.int, 'speed_mph':np.int, 'light_state':np.str, 'seconds_in_zone':np.float, 'vehicles_remaining_in_zone':np.int,  'seconds_of_light_state':np.float, 'seconds_since_green':np.float, 'zone_recent_freeflow_speed_mph':np.int,'zone_recent_freeflow_speed_mph':np.int,
            'seconds_last_green':np.float, 'frame':np.int, 'vehicle_name':np.str}

    elif version == 7.0: # System v5.4 through v6.3
        return {'count_version':np.int, 'site_revision':np.int, 'local_time_str':np.str, 'utc_offset_minutes':np.float, 'turn':np.str, 'length_ft':np.int, 'speed_mph':np.int, 'light_state':np.str, 'seconds_in_zone':np.float, 'vehicles_remaining_in_zone':np.int,  'confidence':np.float}

    elif version == 8.0: # System v6.4 through current
        return {'count_version':np.int, 'site_revision':np.int, 'local_time_str':np.str, 'utc_offset_minutes':np.float, 'turn':np.str, 'length_ft':np.int, 'speed_mph':np.int, 'light_state':np.str, 'seconds_in_zone':np.float, 'vehicles_remaining_in_zone':np.int,  'seconds_of_light_state':np.float, 'seconds_since_green':np.float, 'zone_recent_freeflow_speed_mph':np.int,'zone_calibration_freeflow_speed_mph':np.int}

    else:
        raise Exception("cannot handle version %f" % (version))


def count_value_index_for_key(key, headers):
    """
    Return index of header corresponding to requested value.
    See `counts.md` in `doc` folder.

    Parameters
    ----------
    key : str
        Key for value of interest
    headers : list of str
        Count line headers

    Returns
    -------
    index : int
        Index in headers where key is found, throws if not found
    """
    return headers.index(key)


def zone_counts_dataframe_from_file(zone_counts_file, date_str = None, drop_unused_columns = True):
    """
    Return a pandas Dataframe for the given CSV file. Assumes that the provided file is
    constructed of count lines all of the same version.

    Parameters
    ----------
    zone_counts_file : str
        Full path to a single zone counts file
    date_str : str (optional, default is None, result will be todays date)
        Date string in YYYYMMDD or YYYY-MM-DD format for count file
    drop_unused_columns : bool (optional, default is True)
        If true, drop count_version, site_revision, local_time_str, and utc_offset_minutes

    Returns
    -------
    counts : Dataframe
        Zone count data from the zone counts CSV file
    """
    version = count_version_for_count_file(zone_counts_file)
    names = count_column_names_for_count_version(version)
    dtypes = count_column_dtypes_for_count_version(version)
    counts = pd.read_csv(zone_counts_file, names=names, dtype=dtypes, index_col=False)

    # default to today if no date provided
    if date_str is None:
        date_str = date_string_from_date(datetime.date.today())

    # construct a datetime Series to be the index
    if version>4.0:
        timestamps = counts['local_time_str'].apply(lambda time_str: datetime_from_date_and_time_strings(date_str, time_str))
    else:
        timestamps = counts['utc_datetime_str'].apply(lambda dt_str: datetime_from_iso_string(dt_str))

    timestamps.name = None

    if drop_unused_columns:
        counts.drop('count_version', axis=1, inplace=True)
        counts.drop('site_revision', axis=1, inplace=True)

        if version>4.0:
            counts.drop('local_time_str', axis=1, inplace=True)
            counts.drop('utc_offset_minutes', axis=1, inplace=True)
        else:
            counts.drop('utc_datetime_str', axis=1, inplace=True)
            counts.drop('vehicle_type', axis=1, inplace=True)
            counts.drop('allowable_turns', axis=1, inplace=True)
            counts.drop('seconds_since_last_exit', axis=1, inplace=True)
            counts.drop('day_or_night', axis=1, inplace=True)


    counts.index = timestamps
    return counts
