import os, datetime, time, requests, json
from gtipy.general_util import *
from gtipy.requests_util import *
from gtipy.events_phases_util import *


USE_SSL_CERT_VERIFICATION = False

def site_at_endpoint(endpoint):
    return SiteAPI(endpoint=endpoint)

def site_at_ip_port(ip, port):
    return SiteAPI(endpoint=None, ip=ip, port=port)

class SiteAPI:

    def __init__(self, endpoint = None, ip = None, port = None):
        """
        Class to interact with a GRIDSMART Site via the API as
        documented at:
        https://support.gridsmart.com/support/solutions/articles/27000026973-api-commands

        Parameters
        ----------
        endpoint : string (optional)
            Full endpoint to site. If not provided, user must
            supply IP and port.
        ip : string (optional)
            IP address of site if full endpoint not provided
        port : int or string (optional)
            Port to reach site endpoint, required if full
            endpoint not provided
        """
        
        if endpoint is None and (ip is None or port is None):
            raise Exception("full endpoint or both IP and port required")

        if endpoint:
            self.ip = None
            self.port = None
            if endpoint[-1]=='/':
                self.api_endpoint = endpoint + 'api/'
            else:
                self.api_endpoint = endpoint + '/api/'

        else:
            self.ip = ip
            self.port = str(port)
            self.api_endpoint = site_api_endpoint(ip, port)


        self.api_version = None
        self.get_api_version()

        self.id = None
        self.get_id()

        self.name = None
        self.get_name()

        self.config = None


    def get_id(self, use_cached = True):
        """
        Get the site ID

        Parameters
        ----------
        use_cached : bool (default: True)
            Use the cached version if available, otherwise request from site

        Returns
        -------
        value : string
            The properly-dashified site ID
        """

        if self.id is None or use_cached == False:
            ep = self.api_endpoint + 'system/siteid.json'
            r = requests.get(ep, verify=USE_SSL_CERT_VERIFICATION)
            if r.status_code != 200:
                raise Exception('failed to get site id')

            self.id = dashify_site_or_zone_id(r.json().encode('ascii'))

        return self.id


    def get_api_version(self, use_cached = True):
        """
        Get the site API version

        Parameters
        ----------
        use_cached : bool (default: True)
            Use the cached version if available, otherwise request from site

        Returns
        -------
        value : string
            The API version the site is using
        """

        if self.api_version is None or use_cached == False:

            ep = self.api_endpoint + 'version.json'
            r = requests.get(ep, verify=USE_SSL_CERT_VERIFICATION)
            if r.status_code != 200:
                raise Exception('failed to get site api version')

            self.api_version = r.json().encode('ascii')

        return self.api_version


    def get_name(self, use_cached = True):
        """
        Get the site name, usually defined by the cross-streets

        Parameters
        ----------
        use_cached : bool (default: True)
            Use the cached version if available, otherwise request from site

        Returns
        -------
        value : string
            The site name based on the configuration, usually cross-streets
        """

        if self.name is None or use_cached == False:
            ep = self.api_endpoint + 'sitename.json'
            r = requests.get(ep, verify=USE_SSL_CERT_VERIFICATION)
            if r.status_code != 200:
                raise Exception('failed to get site name')

            self.name = r.json().encode('ascii')

        return self.name


    def get_config(self, use_cached = True):
        """
        Get the full site configuration (JSON)

        Parameters
        ----------
        use_cached : bool (default: True)
            Use the cached version if available, otherwise request from site

        Returns
        -------
        value : dict
            Dictionary created from site config JSON.
        """
        if self.config is None or use_cached == False:
            r_site = requests.get(self.api_endpoint + 'site.json', verify=USE_SSL_CERT_VERIFICATION)
            self.config = r_site.json()

        return self.config


    def get_vehicle_zones(self):
        """
        Get all the vehicle zones for the site

        Parameters
        ----------
        none

        Returns
        -------
        vehicle_zones : dict
            Dictionary of <zone_id, zone_data>
        """
        site_config = self.get_config()
        # <key, value> = <zone_id, dict>
        vehicle_zones = {}
        for cam in site_config['CameraDevices']:
            cam_model = cam.values()[0] # key is fisheye, [0] value is dict
            masks = cam_model['CameraMasks']
            zones = masks['ZoneMasks']
            vzones = filter( (lambda z: z.keys()[0]=='Vehicle'), zones)
            vzones = map( (lambda z: z.values()[0]), vzones) # key is vehicle, [0] value is dict
            for vz in vzones:
                # must dashify b/c our JSON serializer removes dashes from GUIDs
                vz_id = dashify_site_or_zone_id(vz['Id'])
                vehicle_zones[vz_id] = vz

        return vehicle_zones


    def get_vehicle_zones_and_dump_to_file(self, zones_json_file=None):
        """
        Get all the vehicle zones for the site and save them to a JSON file

        Parameters
        ----------
        zones_json_file : str
            Full path to file to write. Will default to './<site_id>/zones.json'

        Returns
        -------
        vehicle_zones : dict
            Dictionary of <zone_id, zone_data>
        zones_json_file : str
            Full path to the file where the zones dict was saved
        """
        zones = self.get_vehicle_zones()
        if zones_json_file is None:
            zones_json_dir = os.path.join( os.path.abspath(os.curdir), self.id )
            create_dir(zones_json_dir)
            zones_json_file = os.path.join(zones_json_dir, 'zones.json')

            with open(zones_json_file, 'w') as f:
                json.dump(zones, f)

        return zones, zones_json_file


    def get_days_of_data_available(self, first_day = None, last_day = None, sort_descending = True):
        """
        Get the list of count days available in given range

        Parameters
        ----------
        first_day : string (defaults to 2010-01-01)
            First day in form YYYY-MM-DD
        last_day : string (defaults to yesterday)
            Last day in form YYYY-MM-DD
        sort_descending : boolean (optional, default = True)
            List is sorted descending by default.

        Returns
        -------
        dates_available : [string]
            List of strings of form 'YYYY-MM-DD'
        """

        r_days_available = requests.get(self.api_endpoint + 'counts.json', verify=USE_SSL_CERT_VERIFICATION)
        if r_days_available.status_code != 200:
            raise Exception('failed to get available dates')

        days_available = r_days_available.json()

        # make ascii rather than unicode
        days_available = map(lambda s: s.encode('ascii'), days_available)


        # handle the date range
        if first_day is None:
            first_date = datetime.date(2010,1,1)
        else:
            first_date = date_from_date_string(first_day)

        if last_day is None:
            last_date = datetime.date.today() - datetime.timedelta(1)
        else:
            last_date = date_from_date_string(last_day)

        yesterday = datetime.date.today() - datetime.timedelta(1)
        if last_date > yesterday:
            last_date = yesterday
    
        dates_available = map(lambda s: date_from_date_string(s), days_available)

        dates_available_in_range = filter(lambda d: d>=first_date and d<=last_date, dates_available)

        days_available = map(lambda d: date_string_from_date(d), dates_available_in_range)

        # keep only through yesterday, sort descending from most recent
        if sort_descending:
            days_available.reverse()

        return days_available
        

    def get_counts_by_date(self, day_str, local_file = None, parse_events = False,
        events_output_file = None):
        """
        Download the requested day of data to the location specified and optionally
        show the events that happened on that day.

        Parameters
        ----------
        day_str : string
            Date of the data to download in form 'YYYY-MM-DD'

        local_file : string (optional, default = None)
            Local file of the downloaded data. If none is provided,
            file will be written to './<site_id>/zips/<YYYY-MM-DD>.zip'

        parse_events : boolean (optional, default = False)
            If true, parse and print out events for the day.

        events_output_file : string (optional, default=None)
            full path to events output file to be written if desired

        Returns
        -------
        success : bool
            Whether or not the download succeeded.
        """
        ep = self.api_endpoint + 'counts/bydate/' + day_str
        if local_file is None:
            site_path = os.path.join( os.path.abspath(os.curdir), self.id )
            create_dir(site_path)
            zips_path = os.path.join( site_path, 'zips' )
            create_dir(zips_path)
            local_file = os.path.join(zips_path, day_str + '.zip')

        r = requests_get_by_chunks(ep, local_file, verify=USE_SSL_CERT_VERIFICATION)

        success = (r.status_code == 200)

        if not success or not parse_events:
            return success
        else:
            print 'Downloaded counts, now parsing events...'
            parse_events_in_counts_zip(local_file, output_file=events_output_file)
            return success


    def get_realtime_data_by_zone(self):
        """
        Get the current realtime data by zone.

        Parameters
        ----------
        none

        Returns
        -------
        realtime_data_by_zone : dict of <string,dict>
            Dictionary keyed by zone ID whose values are a dictionary
            of the various realtime data values. See `doc` folder and/or
            api_constants.py.
        """
        ep = self.api_endpoint + 'realtimedata/zone.json'
        r_rtdz = requests.get(ep, verify=USE_SSL_CERT_VERIFICATION)
        rtdz = r_rtdz.json() # returns list of dicts, one for each zone
        num_zones = len(rtdz)
        realtime_data_by_zone = {}
        for zd in rtdz:
            zone_id = zd['ZoneId']
            realtime_data_by_zone[zone_id] = zd

        return realtime_data_by_zone


    def get_realtime_data_by_phase(self):
        """
        Get the current realtime data by phase.

        Parameters
        ----------
        none

        Returns
        -------
        realtime_data_by_phase : dict of <string,dict>
            Dictionary keyed by phase number whose values are a dictionary
            of the various realtime data values. See `doc` folder and/or
            api_constants.py.
        """
        ep = self.api_endpoint + 'realtimedata/phase.json'
        r_rtdp = requests.get(ep, verify=USE_SSL_CERT_VERIFICATION)
        rtdp = r_rtdp.json() # returns list of dicts, one for each phase 1-16
        num_phases = len(rtdp)
        realtime_data_by_phase = {}
        for pd in rtdp:
            phase_id = pd['Phase']
            realtime_data_by_phase[phase_id] = pd

        return realtime_data_by_phase


def get_site_id(ip, port):
    """
    Get the ID of the site at the provided IP address and optional port
    """
    site = SiteAPI(endpoint=None, ip=ip, port=port)
    return site.get_id()


def get_site_api_version(ip, port):
    """
    Get the API version of the site at the provided IP address and optional port
    """
    site = SiteAPI(endpoint=None, ip=ip, port=port)
    return site.get_api_version()


def get_site_name(ip, port):
    """
    Get the name of the site at the provided IP address and optional port
    """
    site = SiteAPI(endpoint=None, ip=ip, port=port)
    return site.get_name()


def site_api_endpoint(ip, port):
    """
    Utility function to construct site API endpoint from IP address and optional port
    """
    return 'http://' + ip + ':' + str(port) + '/api/'


def load_vehicle_zones_from_file(zones_json_file):
    """
    Get the zones from a JSON file such as written by SiteAPI.get_vehicle_zones_and_dump_to_file
    """
    with open(zones_json_file, 'r') as f:
        zones = json.load(f)
        return zones
