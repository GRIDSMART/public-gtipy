import pytz # for timezone support
from gtipy import * # make sure the gtipy directory is in your python path

# this must be a GRIDSMART site you can see from your network
ip = '192.168.15.232'
port = 8902

# create SiteAPI object using the utility function
site = site_at_ip_port(ip, port)

# show the site names
print ''
print 'Your site is named: ' + site.name

# find out what days of data are available from your site (sorted descending)
data_days_available = site.get_days_of_data_available()

# we'll look only at the last three days 
data_days_available = data_days_available[:min(3,len(data_days_available))]

print ''
print site.name + 'Recent days of data available are: '
for dd in range(len(data_days_available)):
    print data_days_available[dd]
print ''

# download the most recent days of data from the site into ./<site_id>/zips/*.zip
for da in data_days_available:
    print '   downloading ' + da
    site.get_counts_by_date(da)

# Now we have YYYY-MM-DD.zip downloads in the site download directory,
# so let's parse them into some aggregate by-day and by-hour CSVs. First
# we need some additional info...

# we need the zone information - let's download them to ./<site_id>/zones.json
(zones, zones_json_file) = site.get_vehicle_zones_and_dump_to_file()

# once you've grabbed zones.json, you can reload them from file like this
# zones = load_vehicle_zones_from_file('./site_id/zones.json') 

# you'll need to make this correct for your sites, read about pytz
site_tz = pytz.timezone('US/Eastern')

# now make the base CSVs (from api_counts_util)
site_path = os.path.join( os.path.abspath(os.curdir), site.id)
make_daily_count_per_line_csvs(
        site_path = site_path,
        zones = zones,
        site_timezone = site_tz
    )


# Now we have one YYYY-MM-DD.csv for each day that contains the
# data from all the zone files. Look at those CSVs and explore
# the functions in counts_util and events_phases_util for
# customizing what you want to do from here...
