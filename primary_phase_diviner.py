"""
The functions herein are to guess (i.e., "divine") the primary phase
for a zone given that the zone has multiple phases assigned. The
method used is based on a standard 8-phase intersection.
"""

def primary_phase_for_zone(zone):
    """
    Get the assumed primary phase for a zone

    Parameters
    ----------
    zone : dict
        Zone dictionary

    Returns
    -------
    primaryPhase : int
        The determined primary phase
    """

    protectedPhases = zone['ProtectedPhases']
    permissivePhases = zone['PermissivePhases']

    # init to no assigned phase
    primaryPhase = 0

    if len(protectedPhases)==1:
        primaryPhase = protectedPhases[0]
    elif len(protectedPhases)>1:
        primaryPhase = primary_phase_by_turns(protectedPhases, zone)
    elif len(permissivePhases)==1:
        primaryPhase = permissivePhases[0]
    elif len(permissivePhases)>1:
        primaryPhase = primary_phase_by_turns(permissivePhases, zone)

    return primaryPhase


def primary_phase_by_turns(phases, zone):
    """
    Determine the primary phase of a zone using the assigned phases
    and allowable turns. This is a utility function used by
    primary_phase_for_zone and is not intended to be used directly.

    Parameters
    ----------
    phases : array of int
        Array of phases to be evaluated
    zone : dict
        Zone dictionary

    Returns
    -------
    primaryPhase : int
        The determined primary phase
    """


    if len(phases)==1:
        return phases[0]

    """
    /* from Engine::PrimaryPhaseDiviner:
    Traditionally, only right-turn-only zones have multiple protected phases, where
    the even-number phases represent the primary phase and the odd number(s) represent
    the lefts on which vehicles in the right-turn zone can move. For example,
    Phase 2 right might also have protected movement with the Phase 3 (or 7) left and/or
    Phase 5 left.
    */
    """

    # this only works w phases 1-8, so remove higher phases
    _phases = filter(lambda p: p<=8, phases)

    # get the turn types allowed
    tt = zone['TurnType']
    left = tt.count('Left')>0
    thru = tt.count('Straight')>0
    right = tt.count('Right')>0
    right_only = right and (not thru) and (not left)

    if not right_only:
        print 'WARNING: primary_phase_diviner: multiple phases but not right turn only for %s' % (zone['Id'])

    _even_phases = filter(lambda p: p%2==0, _phases)
    if len(_even_phases)==0:
        # no even phases, will log warning and return lowest odd phase
        print 'WARNING: primary_phase_diviner: no even phases for %s' % (zone['Id'])
        _phases.sort()
        return _phases[0]

    elif len(_even_phases)>1:
        _even_phases.sort()
        # log warning if more than one even phase
        print 'WARNING: primary_phase_diviner: multiple even phases for %s' % (zone['Id'])

    # return first even phase
    return _even_phases[0]
