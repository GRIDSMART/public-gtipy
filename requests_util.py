import requests


def requests_get_by_chunks(endpoint, filepath, chunk_size = 32768, verify=True):
    """
    Download by chunks using the requests module

    Parameters
    ----------
    filepath : string
        Full path to file being downloaded (will overwrite existing)

    Returns
    -------
    r : requests.models.Response
        Returns the requests get result
    """

    r = requests.get(endpoint, stream=True, verify=verify)
    with open(filepath, 'wb') as fd:
        for chunk in r.iter_content(chunk_size):
            fd.write(chunk)
    return r
