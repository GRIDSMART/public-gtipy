import numpy as np

def reindex_dataframe_with_incrementing_integers(df, start_idx = 0):
    """
    Make the index of a pandas DataFrame into an increasing integer
    like a simple row counter. This will overwrite the index of the
    input DataFrame in-place.

    Parameters
    ----------
    df : DataFrame
        An singly-indexed pandas DataFrame
    start_idx : int
        Starting integer for new index

    Returns
    -------
    none
    """
    bound_idx = df.index.shape[0]
    df.index = np.arange(start_idx, start_idx + bound_idx)
