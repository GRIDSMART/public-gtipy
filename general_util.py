import os, re, zipfile
import pandas as pd
import datetime, pytz


def create_dir(the_path):
    """
    Create the specified directory if it doesn't exist. Uses
    mkdir but doesn't throw exception if the_path already
    exists.
    """
    pp = os.path.abspath(os.path.expanduser(the_path))
    if not os.path.exists(pp):
        os.mkdir(pp)
    elif not os.path.isdir(pp):
        raise Exception('%s exists and is not a directory' % (pp))


def matching_files_in_dir(query_dir, recursive=False, match_start=None, match_ext=None):
    """
    List of all regular files found in query directory whose filenames
    match the specifications.

    Parameters
    ----------
    query_dir : string
        root directory to search
    recursive : bool (optional, default=False)
        recurse all subdirectories if True
    match_start : string (optional, default=None)
        match only files that start with this string if provided
    match_ext : string (optional, default=None)
        match only files with this extension if provided

    Returns
    -------
    files : list
        list of all the matching files
    """

    files = []

    # ensure ext begins with period to work with os.path.splitext()
    if (match_ext is not None) and (not match_ext.startswith('.')):
        match_ext = '.' + match_ext

    abs_query_dir = os.path.abspath(query_dir)
    for ff in os.listdir(abs_query_dir):

        # create full path by joining dir and ff
        ff_full = os.path.join(abs_query_dir, ff)

        if os.path.isdir(ff_full) and recursive is True:
            # recurse subdir
            files.extend( matching_files_in_dir(ff_full,
                            recursive=recursive,
                            match_start=match_start,
                            match_ext=match_ext)
                )

        elif os.path.isfile(ff_full):
            root, fname = os.path.split(ff_full)
            pname, ext = os.path.splitext(ff_full)

            start_matched = False
            if match_start is None or fname.startswith(match_start):
                start_matched = True

            ext_matched = False
            if match_ext is None or ext.lower() == match_ext.lower():
                ext_matched = True

            if start_matched and ext_matched:
                files.append(ff_full)

    return files


def regex_matching_files_in_dir(query_dir, recursive=False, regex_pattern=None, match_ext=None):
    """
    List of all regular files found in query directory whose filenames
    match the regex pattern and optional extension.

    Parameters
    ----------
    query_dir : string
        root directory to search
    recursive : bool (optional, default=False)
        recurse all subdirectories if True
    regex_pattern : string (optional, default=None)
        match only files whose names match this regex pattern exactly
    match_ext : string (optional, default=None)
        match only files with this extension if provided

    Returns
    -------
    files : list
        list of all the matching files
    """

    files = []

    # ensure ext begins with period to work with os.path.splitext()
    if (match_ext is not None) and (not match_ext.startswith('.')):
        match_ext = '.' + match_ext

    abs_query_dir = os.path.abspath(query_dir)
    for ff in os.listdir(abs_query_dir):

        # create full path by joining dir and ff
        ff_full = os.path.join(abs_query_dir, ff)

        if os.path.isdir(ff_full) and (not os.path.islink(ff_full)) and recursive is True:
            # recurse subdir
            files.extend( regex_matching_files_in_dir(ff_full,
                            recursive=recursive,
                            regex_pattern=regex_pattern,
                            match_ext=match_ext)
                )

        elif os.path.isfile(ff_full):
            fname, ext = os.path.splitext(ff)

            name_matched = False
            if regex_pattern is None:
                name_matched = True
            else:
                m = re.search(regex_pattern, fname)
                if m is not None and m.group(0)==fname:
                    name_matched = True

            ext_matched = False
            if match_ext is None or ext.lower() == match_ext.lower():
                ext_matched = True

            if name_matched and ext_matched:
                files.append(ff_full)

    return files


def net_size_of_dir_contents(the_path):
    """
    Sum of file sizes in the directory. Not recursive.
    """

    pp = os.path.abspath(os.path.expanduser(the_path))
    if not (os.path.exists(pp) and os.path.isdir(pp)):
        raise Exception('%s does not exist or is not a directory' % (pp))

    net_size = 0
    the_files = matching_files_in_dir(the_path)
    for f in the_files:
        net_size += os.path.getsize(os.path.join(the_path,f))

    return net_size


def date_string_from_date(date):
    """
    Return string of form YYYY-MM-DD from date or datetime object
    """
    return '%04d-%02d-%02d' % (date.year, date.month, date.day)
    

def iso_string_from_datetime(dt):
    """
    Return YYYYMMDDThhmmss.ffffff string for datetime
    """
    timestamp = '%04d%02d%02dT%02d%02d%02d' % (
        dt.year,
        dt.month,
        dt.day,
        dt.hour,
        dt.minute,
        dt.second
        )

    if dt.microsecond > 0:
        timestamp = '%s.%d' % (timestamp, dt.microsecond)

    return timestamp


def datetime_from_iso_string(iso_datetime_string):
    """
    Return datetime object for YYYYMMDDThhmmss.ffffff or
    YYYYMMDDThhmmss string
    """
    parts = iso_datetime_string.split('T')
    the_date = date_from_date_string(parts[0])
    the_time = time_from_time_string(parts[1])
    return datetime.datetime.combine(the_date, the_time)


def datetime_from_date_and_time_strings(date_string, time_string=None):
    """
    Return datetime object from YYYYMMDD or YYYY-MM-DD string and 
    HHHMMSS.ffffff or HHHMMSS string. If no time is provided, will
    use noon on the day in question.
    """
    the_date = date_from_date_string(date_string)
    if time_string is not None:
        the_time = time_from_time_string(time_string)
    else:
        the_time = datetime.time(12,0,0)
    return datetime.datetime.combine(the_date, the_time)


def date_from_date_string(date_string):
    """
    Return date object from YYYYMMDD or YYYY-MM-DD string
    """
    ds = date_string.replace('-','') # strip any dashes
    YYYY = int(ds[:4])
    MM = int(ds[4:6])
    DD = int(ds[6:8])
    return datetime.date(YYYY,MM,DD)


def datetime_from_date_string(date_string):
    """
    Return datetime object from YYYYMMDD or YYYY-MM-DD string
    """
    ds = date_string.replace('-','') # strip any dashes
    YYYY = int(ds[:4])
    MM = int(ds[4:6])
    DD = int(ds[6:8])
    return datetime.datetime(YYYY,MM,DD)
    

def time_from_time_string(time_string):
    """
    Return time object from HHMMSS.ffffff or HHHMMSS string
    """
    hh = int(time_string[0:2])
    mm = int(time_string[2:4])
    ss = int(time_string[4:6])
    ffffff = 0
    parts = time_string.split('.')
    if len(parts)>1:
        ffffff = parts[1][:6]
        p = 6 - len(ffffff)
        ffffff = int(ffffff) * (10**p)
    return datetime.time(hh,mm,ss,ffffff)


def unzip(source_filename, dest_dir):
    # from http://stackoverflow.com/questions/12886768/how-to-unzip-file-in-python-on-all-oses
    with zipfile.ZipFile(source_filename) as zf:
        for member in zf.infolist():
            # Path traversal defense copied from
            # http://hg.python.org/cpython/file/tip/Lib/http/server.py#l789
            words = member.filename.split('/')
            path = dest_dir
            for word in words[:-1]:
                drive, word = os.path.splitdrive(word)
                head, word = os.path.split(word)
                if word in (os.curdir, os.pardir, ''): continue
                path = os.path.join(path, word)
            zf.extract(member, path)


def regex_pattern_for_mac_address(with_dashes = True):
    """
    Return regex pattern string that matches MAC addresses such as 00-30-53-15-1f-1c
    """
    pattern = '([0-9a-zA-Z]{2}-){5}[0-9a-zA-Z]{2}'

    if not with_dashes:
        pattern = '([0-9a-zA-Z]{2}){5}[0-9a-zA-Z]{2}'

    return pattern


def regex_pattern_for_date(with_dashes = True):
    """
    Return regex pattern string that matches dates of form YYYY-MM-DD. Doesn't check date
    validity, just that there are digits in that form.
    """
    pattern = '[0-9]{4}-[0-9]{2}-[0-9]{2}'

    if not with_dashes:
        pattern =  '[0-9]{4}[0-9]{2}[0-9]{2}'

    return pattern


def regex_pattern_for_site_or_zone_id(with_dashes = True):
    """
    Return regex pattern string that matches the GUID form we use for site and zone IDs,
    including the dashes, e.g., a3160e1b-f07c-4110-a3da-62087ea14a10
    """
    pattern = '[0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12}'

    if not with_dashes:
        pattern = '[0-9a-zA-Z]{8}[0-9a-zA-Z]{4}[0-9a-zA-Z]{4}[0-9a-zA-Z]{4}[0-9a-zA-Z]{12}'

    return pattern


def dashify_site_or_zone_id(id):
    """
    Our JSON serializer removes the dashes, this puts them back
    """
    # undash first, then add dashes
    idu = id.replace('-','')
    return '%s-%s-%s-%s-%s' % (idu[:8], idu[8:12], idu[12:16], idu[16:20], idu[20:])


def regex_matching_dirs_in_dir(query_dir, regex_pattern):
    """
    List of all directories found in query directory whose names
    match the regex pattern.

    Parameters
    ----------
    query_dir : string
        root directory to search
    regex_pattern : string
        match only directories whose names match this regex pattern exactly

    Returns
    -------
    dirs : list
        list of all the matching directories
    """

    dirs = []

    abs_query_dir = os.path.abspath(query_dir)
    for ff in os.listdir(abs_query_dir):

        # create full path by joining dir and ff
        ff_full = os.path.join(abs_query_dir, ff)

        if os.path.isdir(ff_full):
            dname, ext = os.path.splitext(ff)

            name_matched = False
            m = re.search(regex_pattern, dname)
            if m is not None and m.group(0)==dname:
                name_matched = True

            if name_matched:
                dirs.append(ff_full)

    return dirs


def mph_from_ft_per_sec(spd_ft_per_sec):
    return 3600./5280. * spd_ft_per_sec


def ft_per_sec_from_mph(spd_mph):
    return 5280./3600. * spd_mph


