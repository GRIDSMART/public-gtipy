"""
Extra utility functions to interact with and plot by-day and by-hour
 data created with the functions from `gtipy.counts_util` namely
`make_daily_count_per_line_csvs` and `make_by_day_and_hour_csvs_for_site`
"""
import os
import numpy as np
import pandas as pd; from pandas import Series, DataFrame
from general_util import *


def dataframe_without_holidays(data_df):
    nonhol_ind = (data_df['holiday']==0)
    return data_df[nonhol_ind]


def dataframe_for_days(data_df, days_of_interest, first_day = None, last_day = None, exclude_holidays = True):

    if first_day is None:
        first_date = data_df.index[0]
    else:
        first_date = datetime_from_date_string(first_day)

    if last_day is None:
        last_date = data_df.index[-1]
    else:
        last_date = datetime_from_date_string(last_day)

    rng_ind = (data_df.index>=first_date) & (data_df.index<last_date)
    data_in_rng = data_df[rng_ind]

    dois = map(lambda s: day_to_index_dict()[s], days_of_interest)
    days_ind = [(d in dois) for d in data_in_rng['day_of_week']]
    data_days = data_in_rng[days_ind]

    if exclude_holidays is True:
        return dataframe_without_holidays(data_days)
    else:
        return data_days


def dataframe_for_weekdays(data_df, first_day = None, last_day = None, exclude_holidays = True):
    
    return dataframe_for_days(data_df, weekdays_list(), first_day = first_day, last_day = last_day, exclude_holidays = exclude_holidays)


def dataframe_for_day_of_week(data_df, day_of_week, first_day = None, last_day = None, exclude_holidays = True):

    doi = day_of_week
    if type(day_of_week) is str:
        doi = day_to_index_dict()[day_of_week]

    if first_day is None:
        first_date = data_df.index[0]
    else:
        first_date = datetime_from_date_string(first_day)

    if last_day is None:
        last_date = data_df.index[-1]
    else:
        last_date = datetime_from_date_string(last_day)

    rng_ind = (data_df.index>=first_date) & (data_df.index<last_date)
    data_in_rng = data_df[rng_ind]

    doi_ind = (data_in_rng.day_of_week == doi)
    data_doi = data_in_rng[doi_ind]

    if exclude_holidays is True:
        return dataframe_without_holidays(data_doi)
    else:
        return data_doi


def volume_for_day_of_week(daily, day_of_week, first_day = None, last_day = None, exclude_holidays = True):
    """
    """
    df = dataframe_for_day_of_week(daily, day_of_week, first_day, last_day, exclude_holidays)
    return df.total


def avg_volume_per_hr_for_day_of_week(hourly, day_of_week, first_day = None, last_day = None, exclude_holidays = True):
    """
    Compute the average hourly profile for a given day of the week. Limited to
    days between first day and last day, if provided, where first_day and last_day
    should be strings of YYYY-MM-DD form. If exclude_holidays is True, then
    those days will be withheld from the computation.
    """
    doi = day_of_week
    if type(day_of_week) is str:
        doi = day_to_index_dict()[day_of_week]

    if first_day is None:
        first_date = hourly.index[0]
    else:
        first_date = datetime_from_date_string(first_day)

    if last_day is None:
        last_date = hourly.index[-1]
    else:
        last_date = datetime_from_date_string(last_day)

    hourly_rng = (hourly.index>=first_date) & (hourly.index<last_date)
    hourly_in_rng = hourly[hourly_rng]
    dow_indicator = (hourly_in_rng['day_of_week']==doi)
    hourly_dow = hourly_in_rng[dow_indicator]

    nonhol_indicator = (hourly_dow['holiday']==0)
    hvdf = hourly_dow[nonhol_indicator]
    vols = hvdf['total']

    # take datetime index (D-H) and turn into multi index of (H, D)
    # normalize drops datetime to midnight hour, making datetime like a date
    vols.index = pd.MultiIndex.from_arrays([vols.index.hour, vols.index.normalize()])
    
    avg_volume_per_hr = vols.unstack().mean(axis=1)

    return avg_volume_per_hr


def by_day_and_by_hour_dtypes():
    return {'timestamp':np.str, 'approach':np.str, 'turn':np.str, 'length_ft':np.int, 'speed_mph':np.int, 'phase':np.str, 'light':np.str, 'zone_id':np.str}


def read_by_day_csv(daily_csv_path):
    # must explicitly note that all-number date is string not int
    daily = pd.read_csv(daily_csv_path, dtype=by_day_and_by_hour_dtypes())
    daily.index = pd.DatetimeIndex(map(lambda ts: datetime_from_date_string(ts), daily.timestamp))
    return daily


def read_by_hour_csv(hourly_csv_path):
    hourly = pd.read_csv(hourly_csv_path, dtype=by_day_and_by_hour_dtypes())
    hourly.index = pd.DatetimeIndex(map(lambda ts: datetime_from_iso_string(ts), hourly.timestamp))
    return hourly


def index_to_day_dict():
    return {0:'MO', 1:'TU', 2:'WE', 3:'TH', 4:'FR', 5:'SA', 6:'SU'}


def index_to_day_dict_weekdays():
    return {0:'MO', 1:'TU', 2:'WE', 3:'TH', 4:'FR'}    


def day_to_index_dict():
    D = {}
    for (idx,day) in index_to_day_dict().iteritems():
        D[day] = idx
    return D


def day_to_index_dict_weekdays():
    D = {}
    for (idx,day) in index_to_day_dict_weekdays().iteritems():
        D[day] = idx
    return D


def list_of_holiday_dates_from_holidays_df(holidays_df):
    holidays = []
    for cc in holidays_df.columns[1:]:
        holidays = holidays + list(holidays_df[cc].values)
    return holidays


def set_holidays_in_data(data_df, holidays_df):
    holidays = list_of_holiday_dates_from_holidays_df(holidays_df)
    data_first_datetime = data_df.index[0]
    data_last_datetime = data_df.index[-1]

    for day in holidays:

        date = datetime_from_date_string(day)
        if date < data_first_datetime or date > data_last_datetime:
            continue

        data_df.loc[day,'holiday'] = 1


def set_total_truck_volume_in_data(data_df):
    truck_columns = filter(lambda s: s.find('trk')>-1, data_df.columns)
    data_df['total_trk'] = data_df[truck_columns].sum(axis=1)


def set_total_lefts_in_data(data_df, included_approaches = None):

    # get just perm_lefts
    perm_left_columns = filter(lambda s: s.find('perm_left')>-1, data_df.columns)

    # get all lefts which will include perm_lefts
    all_left_columns = filter(lambda s: s.find('left')>-1, data_df.columns)

    # remove perm
    left_columns = filter(lambda s: s not in perm_left_columns, all_left_columns)

    # remove approaches not in included
    if included_approaches is not None:
        left_columns =  filter(lambda lc: any([lc.find(ia)>-1 for ia in included_approaches]), left_columns)
        perm_left_columns = filter(lambda lc: any([lc.find(ia)>-1 for ia in included_approaches]), perm_left_columns)


    data_df['left_total'] = data_df[left_columns].sum(axis=1)
    data_df['perm_left_total'] = data_df[perm_left_columns].sum(axis=1)


def weekdays_list():
    return  ['MO', 'TU', 'WE', 'TH', 'FR']


def all_days_list():
    return  ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU']
