# GRIDSMART Event Logs

This document describes how the GRIDSMART System records various system-level events.

For systems licensed for the Performance or the Performance Plus Module, event files can be downloaded from the Processor via the [System API](https://support.gridsmart.com/support/solutions/articles/27000026973-api-commands) as part of the *Counts by date* API, or by connecting a USB drive to the Processor.

System events are archived onboard the Processor as `c:\aldis\events\<YYYY-MM-DD>.zip`, one for each day, containing a single CSV file for that day.

Each line in the uncompressed CSV event log represents one event and is of the form `timestamp,code,payload`:
    
    HHmmss.f,C,payload

where `C` is a string integer code from following table and `payload` is a string that contains no commas. The timestamps are in system local time.

The `phases` (215) payload is a 16-character string of `R`, `Y` or `G`  (red, yellow, green) for TS2 and ITS, or of `0`s and `1`s for TS1, where `0` indicates not green and `1` indicates green.


| event              | code | payload    |
| ---                | ---  | ---        |
| server on          | 200  |            |
| server off         | 201  | reason     |
| update started     | 205  |            |
| engine on          | 210  |            |
| engine off         | 211  |            |
| phases changed     | 215  | phases     |
| flash              | 216  |            |
| camera online      | 220  | camera MAC |
| camera offline     | 221  | camera MAC |
| camera learn start | 223  | camera MAC |
| camera learn done  | 224  | camera MAC |
| camera day mode    | 225  | camera MAC |
| camera night mode  | 226  | camera MAC |
| camera loss vis    | 230  | camera MAC |
| camera regain vis  | 231  | camera MAC |
| cameras reboot     | 232  | camera MAC |
| user auth failed   | 239  |            |
| publish            | 240  | (user)     |
| reboot called      | 241  |            |
| WAN changed        | 242  |            |

These codes are separated into the following regions:

| Region              | code      |
| ---                 | ---       |
| Server\Engine       | 200 - 214 |
| Controller\Cabinet  | 215 - 219 |
| Camera              | 220 - 234 |
| API                 | 235 - 249 |
| Free                | 250 - 255 |


