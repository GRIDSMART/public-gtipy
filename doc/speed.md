# GRIDSMART Speed Data

This document describes how GRIDSMART computes and reports speed data as of GRIDSMART 6.4 (November 2016).

## Summary
GRIDSMART computes normalized speeds without resorting to complicated, error-prone, and potentially dangerous camera calibration methods. The user enters the approximate free flow speed for each zone in its Zone Options dialog. The free flow speed is usually the posted speed limit or higher. The default value is 40mph, or 40kph if metric units have been selected in the Site Settings.

## Why and how does GRIDSMART normalize speed?
The raw speed estimates computed by GRIDSMART are subject to significant bias from very small camera tilt and/or road surface deviation from level. Such bias can be corrected statistically using a known, approximate free flow speed.

GRIDSMART observes the zones over a learning period of 14 days and equates the raw free flow speed during that time to the user-entered free flow speed to compute a normalization factor. The raw calibration free flow speed from the first 14 days, along with the most recent free flow speed over the last 14 days, are both recorded in each count line as noted in the count file documentation. Speeds written to the realtime data files and/or returned by the realtime data APIs are already normalized. Note that the computed normalization can be applied to generate accurate speed reports on historical data. Also note that a value of -1 for any speed data element means that no speed data is available.

## Speed Reports
Speed reports are done by cycle for stop-line zones - i.e., those assigned to phases - with a user configurable Start-Up Lost Time. For zones without assigned phases, the speed reports are presented using the selected time interval. No data is shown for cycles or intervals with less than 3 measurements. Speed reports are not shown for zones without allowed thru movements. **IMPORTANT NOTE**: Reports run before the 14 day calibration period is complete may show uncalibrated speed data.
