# GRIDSMART State Logs

This document describes how the GRIDSMART System logs zone state changes.

For systems licensed for the Performance or the Performance Plus Module running GRIDSMART 6.6 or later, the state files can be downloaded from the Processor via the [System API](https://support.gridsmart.com/support/solutions/articles/27000026973-api-commands) as part of the *Counts by date* API, or by connecting a USB drive to the Processor.

Zone state files are archived onboard a Processor as `c:\aldis\states\<YYYY-MM-DD>.zip`, one for each day, where the compressed folder contains one CSV for each zone as described below.

## GRIDSMART v6.6

When a zone call transitions between on and off, or vice versa, that event will be logged to a zone state log file. Active zone state logs are named according to a `<ZONE-ID>.csv` convention, where the `ZONE-ID` is the "Id" attribute specified in the site configuration XML or JSON for the zone. Onboard the Processor zone state files are stored in `c:\aldis\states\<YYYY-MM-DD>` directories, one for each day. Each line in a zone state log represents a transition event and is of the form `timestamp,previous,new`:
    
    HHmmss.f,previous,new

where the timestamp is in system local time, and `previous` and `new` are integers that represent a bit mask characterizing the zone call state.

The least significant bit indicates whether or not the zone call is on (active) or off (inactive), hence any odd integer indicates an active call, while any even integer or 0 indicates inactive. The full enumeration of the bitmask is as follows:

```
enum ZONE_CALL_STATE : int
{
    ZONE_CALL_CLEAR_ALL = 0,
    ZONE_CALL_ON = 1,
    ZONE_CALL_LEARNING = 2,
    ZONE_CALL_DETECTION = 4,
    ZONE_CALL_TIME = 8,
    ZONE_CALL_LATCHING = 16,
    ZONE_CALL_VISIBILITY = 32,
    ZONE_CALL_RECALL = 64,
    ZONE_CALL_USER_SET = 256, // indicates user affected call state
    ZONE_CALL_USER = 512 // if ZONE_CALL_USER_SET, indicates whether user activated or deactivated zone
};
```

The directory of individual zone CSV files for a given day will be archived to `c:\aldis\states\<YYYY-MM-DD>.zip` sometime after midnight on that day.
