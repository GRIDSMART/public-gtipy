# GRIDSMART Zone Arrival Logs

This document describes how the GRIDSMART System logs zone estimated zone arrival events.

For systems licensed for the Performance or the Performance Plus Module running GRIDSMART 6.6 or later, the arrival files can be downloaded from the Processor via the System API as part of the [Counts by date API](https://support.gridsmart.com/support/solutions/articles/27000026973-api-commands), or by connecting a USB drive to the Processor.

Zone arrival files are archived onboard a Processor as `c:\aldis\arrivals\<YYYY-MM-DD>.zip`, one for each day, where the compressed folder contains one CSV for each zone as described below.

## GRIDSMART v6.6

When an object hypothesis arrives in a zone, that arrival will be logged to a zone arrival log file. Zone arrival logs, like zone count files, are named according to a `<ZONE-ID>.csv` convention, where the `ZONE-ID` is the "Id" attribute specified in the site configuration XML or JSON for the zone. Onboard the Processor zone arrival log files are stored in `c:\aldis\arrivals\<YYYY-MM-DD>` directories, one for each day. Each line in a zone arrival log represents a single object arrival in the zone.
    
    HHmmss.f

where the timestamp is in system local time.

The directory of individual zone CSV files for a given day will be archived to `c:\aldis\arrivals\<YYYY-MM-DD>.zip` sometime after midnight on that day.


### Arrivals vs Counts

Arrival data and count data are not necessarily consistent. Counts are computed after observing a vehicle enter and exit the entire field of view and are hence based on longer observations and more confident trajectories. Arrivals are based on shorter observations and object trajectories and are hence subject to more variation.

