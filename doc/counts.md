# GRIDSMART Count Files

This document describes how the what constitutes an *exit event* in the GRIDSMART System and how those events are logged in the count files.

A vehicle exiting a zone is a count event. Each count event is recorded as a single line in the corresponding zone count file. Zone count files are named according to a `<ZONE-GUID>.csv` convention, where the zone GUID is the "Id" attribute specified in the site configuration XML or JSON for the zone. For systems licensed for the Performance or the Performance Plus Module, the count files can be downloaded from the Processor as described in the [GRIDSMART API](https://support.gridsmart.com/support/solutions/articles/27000026973-api-commands) documentation, in particular the *Counts by date* section, or by connecting a USB drive to the Processor.

Please see `speed.md` in this directory to understand more about the speed data beginning with GRIDSMART 6.4.

## GRIDSMART v6.4 (count version 8)
Count Version (8), Site Revision, Local Timestamp (HHMMSS.f format), UTC Offset In Minutes, Turn, Vehicle Length in Feet, Unnormalized Vehicle Speed, Light State, Seconds in Zone, Vehicles Remaining in Zone, Seconds of Light State, Seconds Since Green, Zone Recent Free Flow Speed (-1 if unavailable), Zone Calibration Free Flow Speed (-1 if unavailable)

## GRIDSMART v6.0 (count version 7)
Count Version (7), Site Revision, Local Timestamp (HHMMSS.f format), UTC Offset In Minutes, Turn, Vehicle Length in Feet, Unnormalized Vehicle Speed in MPH, Light State, Seconds in Zone, Vehicles Remaining in Zone, Confidence.

## GRIDSMART v4.0 through v5.4 (count version 4.0)
CountVersion (4.0), SiteRevision, UtcTimestamp (YYYYMMDDTHHMMSS.ffffff format), InternalVehicleId, InternalVehicleType, VehicleLengthFeet, UnnormalizedVehicleSpeedMph, Turn, AllowableTurns, SecondsInZone, SecondsSinceLastExit, EstimatedQueueLength, LightStateOnExit, SecondsSinceGreen, InternalFrameCount, DayOrNight

## GRIDSMART v3.3
Date, TimeWithFractionalSeconds, SecondsOfDay, InternalVehicleId, InternalVehicleType, VehicleLengthFeet, UnnormalizedVehicleSpeedMph, Turn, AllowableTurns, SecondsInZone, SecondsSinceLastExit, EstimatedQueueLength, LightStateOnExit, SecondsSinceGreen, InternalFrameCount, DayOrNight

## GRIDSMART v3.2 and earlier
Date, TimeWithIntegerSeconds, SecondsOfDay, InternalVehicleId, InternalVehicleType, VehicleLengthFeet, UnnormalizedVehicleSpeedMph, Turn, AllowableTurns, SecondsInZone, SecondsSinceLastExit, EstimatedQueueLength, LightStateOnExit, SecondsSinceGreen, InternalFrameCount, DayOrNight