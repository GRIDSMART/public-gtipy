# GRIDSMART Realtime Data Logs

This document describes how the GRIDSMART System stores minute-by-minute, phase-based performance data.

For systems licensed for the Performance or the Performance Plus Module, realtime data files can be downloaded from the Proecssor via the [System API](https://support.gridsmart.com/support/solutions/articles/27000026973-api-commands) as part of the *Counts by date* API, or by connecting a USB drive to the Processor. Recent realtime data can also be accessed with a variety of other System APIs.

Realtime data files are archived onboard the Processor in `c:\aldis\realtime\<YYYY-MM-DD>.zip`, one for each day, containing a single CSV file for that day. Details below refer to lines in the uncompressed CSV files.

## GRIDSMART v6.4 (realtime v2)

Each line in a realtime data file represents one minute of data. The lines are of the form:
    
    version,hhmm,D1_p1,...,D1_p16,D2_p1,...,D2_p16,...,D12_p1,...,D12_p16,D13_p1,...,D13_p16

where `version` indicates the version of the realtime data represented in this line, the `hhmm` timestamp is hours (00-24) and minutes (00-59), and where `D1-D13` represent the 13 different realtime data elements, as indicated below, and where `p1-p16` represent the 16 possible phases. This will result in 210 total columns, one `version`, one for `hhmm`, and 16 for each of the 13 data elements. With the exception of Red Occupancy Time, all data elements are integer valued.


### Realtime Data Elements D1-D13
1. Occupancy Time (0-60 sec)
2. Percent Arrival On Green (0-100)
3. Greens Received (usually 0 or 1)
4. Green Time (0-60 sec)
5. Red Time (0-60 sec)
6. Green Occupancy Time (0-60 sec, <= green time)
7. Red Occupancy Time (0-5 sec, <= red time, for ROR5)
8. Rights (>= 0)
9. Thrus (>= 0)
10. Lefts (>= 0)
11. Uturns (>= 0)
12. Speed (mph, -1 indicates no measurements)
13. Percent Arrival On Red (0-100)


## GRIDSMART v6.3 and earlier (realtime v1)

Each line in the realtime data log represents one minute of data. The lines are of the form:
    
    hhmm,D1_p1,...,D1_p16,D2_p1,...,D2_p16,...,D12_p1,...,D12_p16

where the `hhmm` timestamp is hours (00-24) and minutes (00-59), and where `D1-D12` represent the 12 different realtime data elements, as indicated below, and where `p1-p16` represent the 16 possible phases. This will result in 193 total columns, one for the `hhmm` timestamp and 16 for each of the 12 data elements. All data elements are integer valued.


### Realtime Data Elements D1-D12
1. Occupancy Time (0-60 sec)
2. Percent Arrival On Green (0-100)
3. Greens Received (usually 0 or 1)
4. Green Time (0-60 sec)
5. Red Time (0-60 sec)
6. Green Occupancy Time (0-60 sec, <= green time)
7. Red Occupancy Time (0-5 sec, <= red time, for ROR5)
8. Rights (>= 0)
9. Thrus (>= 0)
10. Lefts (>= 0)
11. Uturns (>= 0)
12. Speed (mph, -1 indicates no measurements)
