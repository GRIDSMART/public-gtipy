class RealtimeDataSnapshotKeys(object):
    CurrentGreenTime = 'CurrentGreenTime' # int, value >= 0
    FillPercent = 'FillPercent' # int, value between 0 and 100 inclusive
    MedianSpeedKph = 'MedianSpeedKph' # int, value either -1 or >= 1
    MedianSpeedMph = 'MedianSpeedMph' # int, value either -1 or >= 1
    OccupancyCount = 'OccupancyCount' # int, value >= 0

class RealtimeDataKeys(object):
    GreenOccupancyTime = 'GreenOccupancyTime' # length 60, values between 0 and 60
    GreensReceived = 'GreensReceived' # length 60, values between 0 and 1 (possibly 2?)
    GreenTime = 'GreenTime' # length 60, values between 0 and 60 inclusive
    MedianSpeedKph = 'MedianSpeedKph' # length 60, values either -1 or >=1
    MedianSpeedMph = 'MedianSpeedMph' # length 60, values either -1 or >=1
    OccupancyTime = 'OccupancyTime' # length 60, values between 0 and 60 inclusive
    PercentArrivalsOnGreen ='PercentArrivalsOnGreen' # length 60, values between 0 and 100 inclusive    
    PercentArrivalsOnRed = 'PercentArrivalsOnRed' # length 60, values between 0 and 100 (6.4+)
    RedOccupancyTime = 'RedOccupancyTime' # length 60, values between 0.0 and 5.0 inclusive
    RedTime = 'RedTime' # length 60, values between 0 and 60 inclusive
    Snapshot = 'Snapshot' # dict, see RealtimeDataByPhaseSnapshotKeys
    Timestamp = 'Timestamp' # string
    TurnsLeft = 'TurnsLeft' # length 60 all >=0 
    TurnsRight = 'TurnsRight' # length 60 all >=0 
    TurnsThrough = 'TurnsThrough' # length 60 all >=0 
    TurnsUturn = 'TurnsUturn' # length 60 all >=0 

    AllTurnKeys = ['TurnsRight', 'TurnsThrough', 'TurnsLeft', 'TurnsUturn']
    ApproachNames = ['Northbound', 'Eastbound', 'Southbound', 'Westbound', 'Unassigned']
    SnapshotKeys = RealtimeDataSnapshotKeys()

class RealtimeDataByZoneKeys(RealtimeDataKeys):
    Arrivals = 'Arrivals' # deprecated, should be empty
    ArrivalsOnGreen = 'ArrivalsOnGreen' # deprecated, should be empty
    Exits = 'Exits' # deprecated, should be length 60 and values all 0
    Outputs = 'Outputs'  # array of ints, could be empty
    PermittedPhases = 'PermittedPhases' # array of ints, could be empty
    ProtectedPhases = 'ProtectedPhases' # array of ints, could be empty
    ZoneApproach = 'ZoneApproach' # string, one of ['Northbound', 'Eastbound', 'Southbound', 'Westbound', 'Unassigned']
    ZoneId = 'ZoneId' # string
    ZoneName = 'ZoneName' # string
    ZoneTurnType = 'ZoneTurnType' # string, CSV of allowable turns

class RealtimeDataByPhaseKeys(RealtimeDataKeys):
    Phase = 'Phase'
