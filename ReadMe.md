# GRIDSMART\public-gtipy

This little repo contains Python code demonstrating how to use the [GRIDSMART Processor API](https://support.gridsmart.com/support/solutions/articles/27000026973-api-commands) to download traffic data files from a GRIDSMART System that is licensed for the Performance or Performance Plus Module.

## Use
Clone the repo to your local machine in a folder called `gtipy` and make sure that that `gtipy` folder is in your Python path. You'll need a well-stocked and recent scientific Python 2.x distribution including `numpy`, `pandas`, `requests`, `matplotlib`, etc. We use and recommend [Anaconda](https://www.continuum.io/why-anaconda).

If you have access to a GRIDSMART site, look at and try `demo.py` and the Jupyter (formerly IPython) notebook `demo-notebook`. Dig into `counts_util.py` and `events_phases_util.py` if you need to customize further or are looking for more features.

Also be sure to look `doc` folder of this repo for more detailed specifications about the contents of the different data files you get from the GRIDSMART System via the API.

Contact support@gridsmart.com if you need more help.