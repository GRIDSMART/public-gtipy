from gtipy.site_api import *
from gtipy.general_util import *

def get_site_data(site, first_day = None, last_day = None, root_data_path = None, result_file = None):
    """
    Get counts from GRIDSMART site. Data for the current day is always ignored since
    it is always incomplete.

    Parameters
    ----------
    site : SiteAPI
        GRIDSMART site API object
    first_day : string (optional, default = 2010-01-01)
        First day to get and upload, in form YYYY-MM-DD
    last_day : string (optional, default = yesterday)
        Last day to get and upload, in form YYYY-MM-DD
    root_data_path : string (defaults to current working directory)
        Directory where site data zip files will be downloaded. A directory
        with the site ID will be created there and files will be downloaded
        into that site ID directory in the provided root path.
    result_file : string (defaults to 'get_site_data_results.csv' in current working directory)
        CSV file where results are written in form `site_ip,site_id,YYYY-MM-DD,get,post` where
        get values will be 0 or 1 to indicate failure or success and post values
        will always be 0.

    Returns
    -------
    None
    """

    # handle the dates
    if first_day is None:
        first_date = datetime.date(2010,1,1)
    else:
        first_date = date_from_date_string(first_day)

    if last_day is None:
        last_date = datetime.date.today() - datetime.timedelta(1)
    else:
        last_date = date_from_date_string(last_day)

    yesterday = datetime.date.today() - datetime.timedelta(1)
    if last_date > yesterday:
        last_date = yesterday

    print '%s to %s for %s (%s)' % (first_date, last_date, site.ip, site.id)

    # default root data path
    if root_data_path is None:
        root_data_path = os.path.abspath('.')

    # default output file
    if result_file is None:
        result_file = 'get_site_data_results.csv'

    # write header
    if not os.path.exists(result_file):
        with open(result_file, 'w') as f:
            f.write('site_ip,site_id,day,get,post\n')


    # set up output directories
    create_dir(root_data_path)
    site_path = os.path.join(root_data_path, site.id)
    create_dir(site_path)

    dates_available = site.get_days_of_data_available()
    for da in dates_available:
        get_status = 0
        post_status = 0

        info_csv = site.ip + ',' + site.id + ',' + da
        
        # only attempt days in provided range
        day = date_from_date_string(da)
        if day < first_date or day > last_date:
            continue

        # log progress
        dts = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print '%s fetching %s from %s (%s)' % (dts, da, site.ip, site.id)

        # local file
        data_file = os.path.join(site_path, da + '.zip')

        get_succeeded = site.get_counts_by_date(da, data_file)

        if get_succeeded:
            get_status = 1

        else:
            print 'GET FAIL'

        # write output line
        with open(result_file, 'a') as f:
            output_line = '%s,%s,%s,%d,%d\n' % (site.ip, site.id, da, get_status, post_status)
            f.write(output_line)

    print 'DONE'
